package com.pmul.weatherapplication;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DatosFragment extends Fragment {

    public DatosFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_datos, container, false);

        Datos datos = MainActivity.getDato(getArguments().getInt("pos"));

        ImageView iconoCielo = view.findViewById(R.id.iconoCielo);
        iconoCielo.setImageResource(datos.getIconoEstadoCielo());
        ImageView iconoTemperatura = view.findViewById(R.id.iconoTemperatura);
        iconoTemperatura.setImageResource(datos.getIconoTendenciaTemperatura());
        TextView provincia = view.findViewById(R.id.nombreProvincia);
        provincia.setText(datos.getNombreProvincia());
        TextView concejo = view.findViewById(R.id.nombreConcejo);
        concejo.setText(datos.getNombreConcejo());
        TextView temperatura = view.findViewById(R.id.temperatura);
        temperatura.setText(String.valueOf(datos.getTemperatura()));
        TextView sensacionTermica = view.findViewById(R.id.sensacionTermica);
        sensacionTermica.setText(String.valueOf(datos.getSensacionTermica()));
        TextView fechaActualizacion = view.findViewById(R.id.fechaActualizacion);
        fechaActualizacion.setText(datos.getFechaHoraActualizacion());

        return view;
    }

}

package com.pmul.weatherapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private ArrayList<String> urls = new ArrayList<String>() {{add("http://servizos.meteogalicia.gal/rss/observacion/rssEstacionsEstActual.action?idEst=10155");add("http://servizos.meteogalicia.gal/rss/observacion/rssEstacionsEstActual.action?idEst=14001");add("http://servizos.meteogalicia.gal/rss/observacion/rssEstacionsEstActual.action?idEst=50500");add("http://servizos.meteogalicia.gal/rss/observacion/rssEstacionsEstActual.action?idEst=10157");add("http://servizos.meteogalicia.gal/rss/observacion/rssEstacionsEstActual.action?idEst=10053");add("http://servizos.meteogalicia.gal/rss/observacion/rssEstacionsEstActual.action?idEst=10156");}};
    private static ArrayList<Datos> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.cargarDatos();
    }

    public static Datos getDato (int pos) {
        return datos.get(pos);
    }

    private void cargarDatos () {
        datos = new ArrayList<>();
        new DowloadXmlTask().execute(urls);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.cargarDatos();
        return true;
    }

    private InputStream openHttpConnection(String url) throws IOException {
        InputStream is = null;
        int responseCode;

        URLConnection connection;
        connection = (new URL(url)).openConnection();

        if(!(connection instanceof HttpURLConnection)) {
            throw new IOException("Not HTTP connection");
        }

        HttpURLConnection httpURLConnection = (HttpURLConnection)connection;
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.connect();
        responseCode = httpURLConnection.getResponseCode();

        if(responseCode == HttpURLConnection.HTTP_OK) {
            is = httpURLConnection.getInputStream();
        }

        return is;
    }

    private class DowloadXmlTask extends AsyncTask<ArrayList<String>, Void, Void> {

        @Override
        protected Void doInBackground(ArrayList<String>[] arrayLists) {
            for (int i = 0; i < arrayLists[0].size(); i++) {
                MainActivity.datos.add(loadWeather(arrayLists[0].get(i)));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (datos.get(0).getFechaHoraActualizacion() != null)
                Snackbar.make(mViewPager, R.string.datos_cargados, Snackbar.LENGTH_SHORT).show();
            else
                Snackbar.make(mViewPager, R.string.error_cargar, Snackbar.LENGTH_SHORT).show();
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }

    }

    private Datos loadWeather(String url) {
        Datos dato = new Datos();

        try {

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            InputStream is = openHttpConnection(url);
            InputStreamReader isr = new InputStreamReader(is);
            parser.setInput(isr);

            int eventType = parser.getEventType();
            int resource = R.drawable.ic_error;

            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG) {

                    switch (parser.getName()) {

                        case "concello": dato.setNombreConcejo(parser.nextText()); break;
                        case "provincia": dato.setNombreProvincia(parser.nextText()); break;
                        case "icoCeo":
                            switch (parser.nextText()){
                                case "101": resource = R.drawable.ic_101_despejado; break;
                                case "103": resource = R.drawable.ic_103_nubes_claros; break;
                                case "105": resource = R.drawable.ic_105_cubierto; break;
                                case "107": resource = R.drawable.ic_107_chuvascos; break;
                                case "111": resource = R.drawable.ic_111_lluvia; break;
                                case "201": resource = R.drawable.ic_201_noche_sin_lluvia; break;
                                case "211": resource = R.drawable.ic_211_noche_con_lluvia; break;
                                case "-9999": resource = R.drawable.ic_error; break;
                            }
                            dato.setIconoEstadoCielo(resource);
                            break;
                        case "icoTemperatura":
                            switch (parser.nextText()){
                                case "400": resource = R.drawable.ic_400_temperaturas_descenso; break;
                                case "401": resource = R.drawable.ic_401_temperaturas_sin_cambios; break;
                                case "402": resource = R.drawable.ic_402_temperaturas_ascenso; break;
                                case "-9999": resource = R.drawable.ic_error; break;
                            }
                            dato.setIconoTendenciaTemperatura(resource);
                            break;
                        case "valTemperatura": dato.setTemperatura(Float.valueOf(parser.nextText())); break;
                        case "valSensTermica": dato.setSensacionTermica(Float.valueOf(parser.nextText())); break;
                        case "dataLocal": dato.setFechaHoraActualizacion(parser.nextText()); break;

                    }
                }

                eventType = parser.next();

            }

            isr.close();
            is.close();

        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        return dato;

    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {}

        public static DatosFragment newInstance(int sectionNumber) {
            DatosFragment fragment = new DatosFragment();
            Bundle args = new Bundle();
            args.putInt("pos", sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_datos, container, false);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 6 total pages.
            return 6;
        }
    }
}

package com.pmul.weatherapplication;

/**
 * Created by Noel on 25/11/2017.
 */

public class Datos {

    private String nombreConcejo;
    private String nombreProvincia;
    private int iconoEstadoCielo;
    private int iconoTendenciaTemperatura;
    private float temperatura;
    private float sensacionTermica;
    private String fechaHoraActualizacion;

    public Datos() {}

    public Datos(String nombreConcejo, String nombreProvincia, int iconoEstadoCielo, int iconoTendenciaTemperatura, float temperatura, float sensacionTermica, String fechaHoraActualizacion) {
        this.nombreConcejo = nombreConcejo;
        this.nombreProvincia = nombreProvincia;
        this.iconoEstadoCielo = iconoEstadoCielo;
        this.iconoTendenciaTemperatura = iconoTendenciaTemperatura;
        this.temperatura = temperatura;
        this.sensacionTermica = sensacionTermica;
        this.fechaHoraActualizacion = fechaHoraActualizacion;
    }

    public String getNombreConcejo() {
        return nombreConcejo;
    }

    public void setNombreConcejo(String nombreConcejo) {
        this.nombreConcejo = nombreConcejo;
    }

    public String getNombreProvincia() {
        return nombreProvincia;
    }

    public void setNombreProvincia(String nombreProvincia) {
        this.nombreProvincia = nombreProvincia;
    }

    public int getIconoEstadoCielo() {
        return iconoEstadoCielo;
    }

    public void setIconoEstadoCielo(int iconoEstadoCielo) {
        this.iconoEstadoCielo = iconoEstadoCielo;
    }

    public int getIconoTendenciaTemperatura() {
        return iconoTendenciaTemperatura;
    }

    public void setIconoTendenciaTemperatura(int iconoTendenciaTemperatura) {
        this.iconoTendenciaTemperatura = iconoTendenciaTemperatura;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public float getSensacionTermica() {
        return sensacionTermica;
    }

    public void setSensacionTermica(float sensacionTermica) {
        this.sensacionTermica = sensacionTermica;
    }

    public String getFechaHoraActualizacion() {
        return fechaHoraActualizacion;
    }

    public void setFechaHoraActualizacion(String fechaHoraActualizacion) {
        this.fechaHoraActualizacion = fechaHoraActualizacion;
    }
}
